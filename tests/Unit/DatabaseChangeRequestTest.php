<?php

use Exception;
use PHPUnit\Framework\TestCase;
use Skunk\DatabaseChangeRequest;
use Skunk\MySqlConnection;
use Skunk\SkunkException;

class DatabaseChangeRequestTest extends TestCase
{

  private DatabaseChangeRequest $dbcrCommand;
  private MySqlConnection       $mySqlConnection;

  protected function setUp(): void
  {
    parent::setUp();

    $this->mySqlConnection = $this->createMock(MySqlConnection::class);
    $this->dbcrCommand     = new DatabaseChangeRequest('test_db', $this->mySqlConnection);
  }

  public function testPrintHelpNoError(): void
  {
    $this->expectOutputString("Usage: php command.php dbcr [OPTION]\n"
      . "Executes database change requests (DBCRs) by checking for and running any DBCR definitions not successfully run previously.\n"
      . "Optional flags can be provided to run a specific dbcr by name, roll a dbcr back by name, or roll back up to a certain number of last run dbcrs.\n"
      . "----------------------------\n"
      . "Options:\n"
      . "	--rollback [string|int]\n"
      . "		- If provided a string the script will look for and attempt to roll back a dbcr keyed that string.\n"
      . "		- If provided a number the script will roll back the most recent DBCRs up-to the given count.\n"
      . "	--help|-h\n"
      . "		- Displays this help text\n");

    $this->expectException(SkunkException::class);
    $this->expectExceptionMessage('Command execution exiting with code: 0');

    $this->dbcrCommand->printHelp();
  }

  public function testPrintHelpWithError(): void
  {
    $this->expectOutputString("Invalid command usage: uh oh!\n"
      . "----------------------------\n"
      . "Usage: php command.php dbcr [OPTION]\n"
      . "Executes database change requests (DBCRs) by checking for and running any DBCR definitions not successfully run previously.\n"
      . "Optional flags can be provided to run a specific dbcr by name, roll a dbcr back by name, or roll back up to a certain number of last run dbcrs.\n"
      . "----------------------------\n"
      . "Options:\n"
      . "	--rollback [string|int]\n"
      . "		- If provided a string the script will look for and attempt to roll back a dbcr keyed that string.\n"
      . "		- If provided a number the script will roll back the most recent DBCRs up-to the given count.\n"
      . "	--help|-h\n"
      . "		- Displays this help text\n");

    $this->expectException(SkunkException::class);
    $this->expectExceptionMessage('Command execution exiting with code: 0');

    $this->dbcrCommand->printHelp('uh oh!');
  }

  public function testRollback(): void
  {
    $this->expectOutputString(
      "\tChecking for DBCR definitions to rollback...\n"
        . "\t- Rolling back DBCR: create_users_table\n"
    );
    $this->mySqlConnection->expects($this->exactly(4))->method('execute')
      ->withConsecutive(
        [
          "select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1",
          ['test_db', DatabaseChangeRequest::DBCR_TABLE_NAME],
        ],
        [
          sprintf(
            'select id, dbcr from %s where status = ? and dbcr = ? limit 1',
            DatabaseChangeRequest::DBCR_TABLE_NAME
          ),
          [true, 'create_users_table'],
        ],
        [
          'drop table users;',
        ],
        [
          'update database_change_requests set status = ? where id = ?',
          [false, 1],
        ]
      )->willReturnOnConsecutiveCalls(
        [['records' => 1]],
        [['id' => 1, 'dbcr' => 'create_users_table']],
        true,
        true
      );

    $this->dbcrCommand->setOptions(['rollback' => 'create_users_table'])->run();
  }

  public function testRollbackNoRecords(): void
  {
    $this->expectException(SkunkException::class);
    $this->expectExceptionMessage('Unable to identify successfully run DBCRs to rollback for "--rollback 5');

    $this->expectOutputString("\tChecking for DBCR definitions to rollback...\n");
    $this->mySqlConnection->expects($this->exactly(2))->method('execute')
      ->withConsecutive(
        [
          "select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1",
          ['test_db', DatabaseChangeRequest::DBCR_TABLE_NAME],
        ],
        [
          sprintf(
            'select id, dbcr from %s where status = ? order by executed_at desc, id desc limit 5',
            DatabaseChangeRequest::DBCR_TABLE_NAME
          ),
          [true],
        ]
      )->willReturnOnConsecutiveCalls(
        [['records' => 1]],
        []
      );

    $this->dbcrCommand->setOptions(['rollback' => 5])->run();
  }

  public function testRollbackNoRollbackDefinition(): void
  {
    $this->expectException(SkunkException::class);
    $this->expectExceptionMessage('DBCR rollback definition for "hacky-dee-doo" is missing. Unable to proceed.');

    $this->expectOutputString("\tChecking for DBCR definitions to rollback...\n");
    $this->mySqlConnection->expects($this->exactly(2))->method('execute')
      ->withConsecutive(
        [
          "select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1",
          ['test_db', DatabaseChangeRequest::DBCR_TABLE_NAME],
        ],
        [
          sprintf(
            'select id, dbcr from %s where status = ? and dbcr = ? limit 1',
            DatabaseChangeRequest::DBCR_TABLE_NAME
          ),
          [true, 'hacky-dee-doo'],
        ]
      )->willReturnOnConsecutiveCalls(
        [['records' => 1]],
        [['id' => 1, 'dbcr' => 'hacky-dee-doo']]
      );

    $this->dbcrCommand->setOptions(['rollback' => 'hacky-dee-doo'])->run();
  }

  public function testRollbackNoRollbackFailedException(): void
  {
    $this->expectException(Exception::class);

    $this->expectOutputString(
      "\tChecking for DBCR definitions to rollback...\n"
        . "\t- Rolling back DBCR: create_users_table\n"
    );
    $this->mySqlConnection->expects($this->exactly(3))->method('execute')
      ->withConsecutive(
        [
          "select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1",
          ['test_db', DatabaseChangeRequest::DBCR_TABLE_NAME],
        ],
        [
          sprintf(
            'select id, dbcr from %s where status = ? and dbcr = ? limit 1',
            DatabaseChangeRequest::DBCR_TABLE_NAME
          ),
          [true, 'create_users_table'],
        ],
        [
          'drop table users;',
        ]
      )->willReturnOnConsecutiveCalls(
        [['records' => 1]],
        [['id' => 1, 'dbcr' => 'create_users_table']],
        $this->throwException(new Exception('Oh noes!'))
      );

    $this->dbcrCommand->setOptions(['rollback' => 'create_users_table'])->run();
  }

  public function testRunDbcr(): void
  {
    $this->expectOutputString(
      "\tChecking for new DBCR definitions to execute...\n"
        . "\t- Running DBCR: create_users_table\n"
    );
    $this->mySqlConnection->expects($this->exactly(4))->method('execute')
      ->withConsecutive(
        [
          "select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1",
          ['test_db', DatabaseChangeRequest::DBCR_TABLE_NAME],
        ],
        [
          sprintf(
            'select count(*) as records from %s where dbcr = ? and status = ? limit 1',
            DatabaseChangeRequest::DBCR_TABLE_NAME
          ),
          ['create_users_table', true],
        ],
        ["create table users ( id int unsigned primary key auto_increment, first_name varchar(255) not null, last_name varchar(255) default '', email varchar(255) not null, password varchar(255) not null, locked_out_at datetime default null, created_at datetime default CURRENT_TIMESTAMP, modified_at datetime default null on update CURRENT_TIMESTAMP, deleted_at datetime default null, constraint email_unique unique (email) );"],
        [
          sprintf('insert into %s (dbcr, status) values (?, ?)', DatabaseChangeRequest::DBCR_TABLE_NAME),
          ['create_users_table', true],
        ]
      )->willReturnOnConsecutiveCalls(
        [['records' => 1]],
        [['records' => 0]],
        true,
        true
      );

    $this->assertEquals(0, $this->dbcrCommand->run());
  }

  public function testRunDbcrCheckRunHistoryException(): void
  {
    $this->expectException(Exception::class);
    $this->expectExceptionMessage('Unable to confirm state of DBCR create_users_table');

    $this->expectOutputString("\tChecking for new DBCR definitions to execute...\n");
    $this->mySqlConnection->expects($this->exactly(2))->method('execute')
      ->withConsecutive(
        [
          "select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1",
          ['test_db', DatabaseChangeRequest::DBCR_TABLE_NAME],
        ],
        [
          sprintf(
            'select count(*) as records from %s where dbcr = ? and status = ? limit 1',
            DatabaseChangeRequest::DBCR_TABLE_NAME
          ),
          ['create_users_table', true],
        ]
      )->willReturnOnConsecutiveCalls(
        [['records' => 1]],
        false
      );

    $this->assertEquals(0, $this->dbcrCommand->run());
  }

  public function testRunDbcrFailedException(): void
  {
    $this->expectException(Exception::class);

    $this->expectOutputString(
      "\tChecking for new DBCR definitions to execute...\n"
        . "\t- Running DBCR: create_users_table\n"
    );
    $this->mySqlConnection->expects($this->exactly(4))->method('execute')
      ->withConsecutive(
        [
          "select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1",
          ['test_db', DatabaseChangeRequest::DBCR_TABLE_NAME],
        ],
        [
          sprintf(
            'select count(*) as records from %s where dbcr = ? and status = ? limit 1',
            DatabaseChangeRequest::DBCR_TABLE_NAME
          ),
          ['create_users_table', true],
        ],
        ["create table users ( id int unsigned primary key auto_increment, first_name varchar(255) not null, last_name varchar(255) default '', email varchar(255) not null, password varchar(255) not null, locked_out_at datetime default null, created_at datetime default CURRENT_TIMESTAMP, modified_at datetime default null on update CURRENT_TIMESTAMP, deleted_at datetime default null, constraint email_unique unique (email) );"],
        [
          sprintf('insert into %s (dbcr, status) values (?, ?)', DatabaseChangeRequest::DBCR_TABLE_NAME),
          ['create_users_table', false],
        ]
      )->will(
        $this->onConsecutiveCalls(
          [['records' => 1]],
          [['records' => 0]],
          $this->throwException(new Exception('Oh noes!')),
          true
        )
      );

    $this->assertEquals(0, $this->dbcrCommand->run());
  }

  public function testRunDbcrPreviouslyExecuted(): void
  {
    $this->expectOutputString("\tChecking for new DBCR definitions to execute...\n");
    $this->mySqlConnection->expects($this->exactly(2))->method('execute')
      ->withConsecutive(
        [
          "select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1",
          ['test_db', DatabaseChangeRequest::DBCR_TABLE_NAME],
        ],
        [
          sprintf(
            'select count(*) as records from %s where dbcr = ? and status = ? limit 1',
            DatabaseChangeRequest::DBCR_TABLE_NAME
          ),
          ['create_users_table', true],
        ]
      )->willReturnOnConsecutiveCalls(
        [['records' => 1]],
        [['records' => 1]]
      );

    $this->assertEquals(0, $this->dbcrCommand->run());
  }

  public function testRunDbcrTableCheckFailed(): void
  {
    $this->expectException(SkunkException::class);
    $this->expectExceptionMessage('Unable to confirm presence of database change request table.');

    $this->mySqlConnection->expects($this->once())->method('execute')
      ->with(
        "select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1",
        ['test_db', DatabaseChangeRequest::DBCR_TABLE_NAME]
      )->willReturn(false);

    $this->assertEquals(1, $this->dbcrCommand->run());
  }

  public function testRunDbcrTableNotExists(): void
  {
    $this->expectOutputString("\tChecking for new DBCR definitions to execute...\n");
    $this->mySqlConnection->expects($this->exactly(3))->method('execute')
      ->withConsecutive(
        [
          "select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1",
          ['test_db', DatabaseChangeRequest::DBCR_TABLE_NAME],
        ],
        [
          "create table database_change_requests (id int auto_increment primary key, dbcr varchar(255) not null, status tinyint(1) default 0 not null, executed_at datetime default current_timestamp() not null);",
        ],
        // This query wouldn't naturally return a positive count here since it couldn't have been previously run.
        // We are only limiting the scope of this test
        [
          sprintf(
            'select count(*) as records from %s where dbcr = ? and status = ? limit 1',
            DatabaseChangeRequest::DBCR_TABLE_NAME
          ),
          ['create_users_table', true],
        ]
      )->willReturnOnConsecutiveCalls(
        [['records' => 0]],
        true,
        [['records' => 1]]
      );

    $this->assertEquals(0, $this->dbcrCommand->run());
  }
}
