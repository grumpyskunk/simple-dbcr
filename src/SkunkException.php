<?php

namespace Skunk;

use Exception;
use Throwable;

class SkunkException extends Exception
{

  /**
   * @param string         $message
   * @param int            $code
   * @param Throwable|null $previous
   */
  public function __construct(
    string    $message = '',
    int       $code = 0,
    Throwable $previous = null
  ) {
    $this->code    = empty($code) ? 500 : $code;
    $this->message = empty($message) ? 'Unexpected error has occurred' : $message;

    parent::__construct($this->message, $this->code, $previous);
  }
}
