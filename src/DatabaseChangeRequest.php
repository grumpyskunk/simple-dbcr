<?php

namespace Skunk;

use Exception;
use JsonException;

class DatabaseChangeRequest extends AbstractCommand
{

  public const DBCR_TABLE_NAME = 'database_change_requests';

  public array             $longOptions = ['rollback:'];
  private string           $database;
  private array            $databaseChangeRequests;
  private MySqlConnection $dbConnection;

  /**
   * @throws Exception
   */
  public function __construct(string $database, MySqlConnection $mySqlConnection)
  {
    $this->database = $database;

    // Establish DB connection
    $this->dbConnection = $mySqlConnection;

    // Get dbcrs file
    $routesFile = file_get_contents(__DIR__ . '/dbcr.json');
    // $routesFile = file_get_contents(__DIR__ . '/dbcr.yml');
    // Parse and assign
    $this->databaseChangeRequests = json_decode($routesFile, true, 512, JSON_THROW_ON_ERROR);
    // $this->databaseChangeRequests = yaml_parse($routesFile);
  }

  /**
   * Executes query to create the change request table
   *
   * @return void
   */
  private function createChangeRequestTable(): void
  {
    $sql = "create table database_change_requests (id int auto_increment primary key, dbcr varchar(255) not null, status tinyint(1) default 0 not null, executed_at datetime default current_timestamp() not null);";

    $this->dbConnection->execute($sql);
  }

  /**
   * Query the database change request table for a given DBCR to evaluate if it has been previously run successfully
   *
   * @param string $dbcr
   *
   * @return bool
   * @throws SkunkException
   */
  private function databaseChangeRequestExecuted(string $dbcr): bool
  {
    $sql    = 'select count(*) as records from ' . self::DBCR_TABLE_NAME . ' where dbcr = ? and status = ? limit 1';
    $result = $this->dbConnection->execute($sql, [$dbcr, true]);

    if (!$result) {
      throw new SkunkException('Unable to confirm state of DBCR ' . $dbcr);
    }

    return (bool) $result[0]['records'];
  }

  /**
   * Query the database to evaluate if the database change request table exists
   *
   * @return bool
   * @throws SkunkException
   */
  private function databaseChangeRequestTableExists(): bool
  {
    $sql    = 'select count(*) as records from information_schema.tables where TABLE_SCHEMA = ? and TABLE_NAME = ? limit 1';
    $result = $this->dbConnection->execute($sql, [$this->database, self::DBCR_TABLE_NAME]);

    if (!$result) {
      throw new SkunkException('Unable to confirm presence of database change request table.');
    }

    return (bool) $result[0]['records'];
  }

  /**
   * @param string $error
   *
   * @return void
   * @throws JsonException|SkunkException
   */
  public function printHelp(string $error = ''): void
  {
    if ($error) {
      $this->pout(sprintf('Invalid command usage: %s', $error));
      $this->pout('----------------------------');
    }

    $this->pout('Usage: php command.php dbcr [OPTION]');
    $this->pout('Executes database change requests (DBCRs) by checking for and running any DBCR definitions not '
      . 'successfully run previously.');
    $this->pout('Optional flags can be provided to run a specific dbcr by name, roll a dbcr back by name, or roll back '
      . 'up to a certain number of last run dbcrs.');
    $this->pout('----------------------------');
    $this->pout('Options:');
    $this->pout("\t--rollback [string|int]");
    $this->pout("\t\t- If provided a string the script will look for and attempt to roll back a dbcr keyed that string.");
    $this->pout("\t\t- If provided a number the script will roll back the most recent DBCRs up-to the given count.");
    $this->pout("\t--help|-h");
    $this->pout("\t\t- Displays this help text");

    $this->terminate();
  }

  /**
   * @param $rollback
   *
   * @return void
   * @throws JsonException
   * @throws SkunkException
   */
  private function rollback($rollback): void
  {
    $this->pout("\tChecking for DBCR definitions to rollback...");
    $sql          = 'select id, dbcr from ' . self::DBCR_TABLE_NAME . ' where status = ?';
    $placeholders = [true];

    if (is_numeric($rollback)) {
      $sql .= ' order by executed_at desc, id desc limit ' . $rollback;
    } else {
      $sql            .= ' and dbcr = ? limit 1';
      $placeholders[] = $rollback;
    }

    $toRollback = $this->dbConnection->execute($sql, $placeholders);
    // Verify that we have any dbcrs to roll back
    if (empty($toRollback)) {
      throw new SkunkException(sprintf(
        'Unable to identify successfully run DBCRs to rollback for "--rollback %s"',
        $rollback
      ));
    }

    // Verify that all dbcrs we are attempting to roll back has a rollback definition.
    foreach ($toRollback as $record) {
      if (!isset($this->databaseChangeRequests[$record['dbcr']]['rollback'])) {
        throw new SkunkException(
          sprintf(
            'DBCR rollback definition for "%s" is missing. Unable to proceed.',
            $record['dbcr']
          )
        );
      }
    }

    // Do the actual rollback
    foreach ($toRollback as $record) {
      $this->pout(sprintf("\t- Rolling back DBCR: %s", $record['dbcr']));

      $rollbackSql = $this->databaseChangeRequests[$record['dbcr']]['rollback'];
      $this->dbConnection->execute($rollbackSql);

      $sql = 'update ' . self::DBCR_TABLE_NAME . ' set status = ? where id = ?';
      $this->dbConnection->execute($sql, [false, $record['id']]);
    }
  }

  /**
   * Executes the database change request command
   *
   * @return int
   * @throws SkunkException|JsonException|Exception
   */
  public function run(): int
  {
    if (!$this->databaseChangeRequestTableExists()) {
      $this->createChangeRequestTable();
    }

    if (empty($this->inputOptions)) {
      $this->runLatest();
    } elseif (isset($this->inputOptions['rollback'])) {
      $this->rollback($this->inputOptions['rollback']);
    }

    return 0;
  }

  /**
   * @throws Exception
   */
  private function runLatest(): void
  {
    $this->pout("\tChecking for new DBCR definitions to execute...");
    foreach ($this->databaseChangeRequests as $name => $definition) {
      if (!$this->databaseChangeRequestExecuted($name)) {
        $this->pout(sprintf("\t- Running DBCR: %s", $name));

        $status = true;
        $error  = null;
        try {
          $dbcrSql = $definition['execute'];
          $this->dbConnection->execute($dbcrSql);
        } catch (Exception $exception) {
          $status = false;
          $error  = $exception;
        } finally {
          $sql = 'insert into ' . self::DBCR_TABLE_NAME . ' (dbcr, status) values (?, ?)';
          $this->dbConnection->execute($sql, [$name, $status]);

          if ($error) {
            throw $error;
          }
        }
      }
    }
  }
}

