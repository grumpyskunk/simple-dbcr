<?php

namespace Skunk;

use JsonException;

abstract class AbstractCommand
{

  const EXECUTE_ERR = 1;
  const EXECUTE_OK  = 0;
  const OPTION_LONG           = '--';
  const OPTION_SHORT          = '-';
  const OPTION_VALUE_EXPECTED = ':';
  const OPT_LONG_HELP  = 'help';
  const OPT_SHORT_HELP = 'h';
  protected array $inputOptions = [];
  protected array $longOptions  = [];
  protected array $shortOptions = [];

  /**
   * Parses input options from the CLI used in command execution
   *
   * @param array $options Command input options to process
   *
   * @return $this
   * @throws SkunkException
   */
  public function parseOptions(array $options): self
  {
    $inputOptions = [];

    $len = count($options);
    // Intentionally using a for loop here to facilitate manually skipping loop positions
    // Intentionally not using get_opt() to give full control over testability and the assigned value of options
    //   i.e. PHP assigns a value of false to optionless input parameters
    for ($index = 0; $index < $len; $index++) {
      // Initialize our option by index and an empty option value
      $option      = $options[$index];
      $optionValue = null;
      // Our option bucket will ultimately point to either the short or long options list if a match is made
      $validOptions = [];

      // Evaluate if short or long option
      if (str_starts_with($option, self::OPTION_LONG)) {
        $option       = str_replace(self::OPTION_LONG, '', $option); // --help becomes help
        $validOptions = &$this->longOptions;
      } elseif (str_starts_with($option, self::OPTION_SHORT)) {
        $option       = str_replace(self::OPTION_SHORT, '', $option); // -h becomes h
        $validOptions = &$this->shortOptions;
      } else {
        // Unexpected option sequence - skip and move on
        $inputOptions['rest'][] = $option;
        continue;
      }

      // Evaluate if the option value was provided as a direct assignment e.g. --color="red"
      $optionParts = explode('=', $option, 2);
      if (count($optionParts) === 2) {
        [$option, $optionValue] = $optionParts;

        // Standardize how we evaluate for empty string "color="
        $optionValue = $optionValue !== '' ? $optionValue : null;
      }

      // If option is help flag...
      if ($option === self::OPT_SHORT_HELP || $option === self::OPT_LONG_HELP) {
        // Print help and terminate
        $this->printHelp();

        $this->terminate();
      }

      foreach ($validOptions as $validOption) {
        // An option value is required when the option ends with a colon
        $valueExpected = str_ends_with($validOption, self::OPTION_VALUE_EXPECTED);

        // If the input option matches this valid option (requirement flag trimmed)...
        if ($option === rtrim($validOption, self::OPTION_VALUE_EXPECTED)) {
          // If a value is expected but was not given by assignment...
          if ($valueExpected && $optionValue === null) {
            // Fetch the next option by index, coalesce if unset/OOB
            $nextOption = $options[$index + 1] ?? null;
            // If our next option is not an option flag...
            $optionValue = (
              $nextOption
              && !str_starts_with($nextOption, self::OPTION_LONG)
              && !str_starts_with($nextOption, self::OPTION_SHORT)
            ) ? $nextOption : null;

            // If we still don't have our required option...
            if ($optionValue === null) {
              $this->printHelp(sprintf('Value expected for option "%s" but none given.', $option));
              $this->terminate(self::EXECUTE_ERR);
            }

            $index += ((int) $optionValue !== null); // Increment our option list index based on the success of finding a value
          }
          // Set our input option value coalesce to boolean true to indicate a non-valued option was passed
          $inputOptions[$option] = $optionValue ?? true;
          // We can stop iterating valid options
          break;
        }
      }
    }

    $this->inputOptions = $inputOptions;

    return $this;
  }

  /**
   * Basic PHP-out via echo -- suppressed in "test" environments
   *
   * @param $input
   *
   * @return void
   * @throws JsonException
   */
  protected function pout($input): void
  {
    if (is_string($input)) {
      echo $input . PHP_EOL;

      return;
    }
    // Encode and print
    echo json_encode($input, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT) . PHP_EOL;
  }

  abstract public function printHelp(string $error = ''): void;

  abstract public function run(): int;

  public function setOptions(array $options): self
  {
    $this->inputOptions = $options;

    return $this;
  }

  /**
   * Testable exit path for command execution
   *
   * @param int $code
   *
   * @return void
   * @throws SkunkException
   */
  protected function terminate(int $code = self::EXECUTE_OK): void
  {
    if (defined('ENVIRONMENT') && ENVIRONMENT === 'test') {
      throw new SkunkException('Command execution exiting with code: ' . $code);
    }

    // @codeCoverageIgnoreStart
    // Calls to exit() cannot be tested
    exit($code);
    // @codeCoverageIgnoreEnd
  }
}

