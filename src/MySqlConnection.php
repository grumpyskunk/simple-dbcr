<?php

namespace Skunk;

/**
 * Stub class exists only as a type-hint mockable
 */
class MySqlConnection
{

  public function execute(string $sql, array $parameters = []): bool|array|null|int
  {
    return true;
  }
}
