# Simple DBCR (Database Change Request)


## About

This is a simple utility script that I have created as a demonstration for managing
schema and data state changes intended for using during application deployments.
This script is by no means a complete solution to all use-cases and will require
individualized adjustments and fixes as necessary.

## Disclaimer

- This demo code was written using PHP 8.1 and includes native methods and type-hinting
that may not be backwards compatible without alteration (tho the requirements to do so
should be relatively minimal)
- This demo code makes some assumptions regarding DB architecture / type that were used
for the purpose of expediency rather than completeness

# Released under MIT License

Copyright (c) 2024 James A Neiderhiser Jr.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
